package com.pluralsight.repository;

import java.util.List;

import com.pluralsight.model.Activity;

public interface ActivityRespository {

	List<Activity> findAllActivities();

	Activity findActivity(String id);

}